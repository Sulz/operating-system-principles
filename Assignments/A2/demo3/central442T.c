#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

// #define CENTRAL_MAILBOX 3436413    //Central Mailbox number 
#define NUM_PROCESSES 4          //Total number of external processes

struct {
  long priority;         //message priority
  int temp;             //temperature
  int pid;                //process id
  int stable;            //boolean for temperature stability
} msgp1, msgp2, cmbox1, cmbox2;

struct group{
  int initTemp;
  int msqid[NUM_PROCESSES];
  int mbox;
  int msqidC;
  int tempAry[NUM_PROCESSES];
} process;


//This function handles a group of processes, in a thread.
//@param: struct group gets passed.
void * stabalize(void * param)
{
  int stable;
  int unstable = 1;
  int sumTemp = 0;
  int result;

  printf("init temp = %d\n", process.initTemp);

  int length = sizeof(msgp1) - sizeof(long);

  struct group * process = (struct group *)param;

  while(unstable == 1){
    int sumTemp = 0;        //sum up the temps as we loop
    int stable = 1;            //stability trap

    // Get new messages from the processes
    for(int i = 0; i < NUM_PROCESSES; i++)
    {
        // printf("L2: Next process.\n");

        result = msgrcv(process->msqidC, &cmbox2, length, 1, 0);
        // printf("Process %d has temp: %d.\n", cmbox2.pid, cmbox2.temp);

        /* If any of the new temps are different from the old temps then we are still unstable. Set the 
            new temp to the corresponding process ID in the array */
        if(process->tempAry[(cmbox2.pid - 1)] != cmbox2.temp) {
              // printf("L2: Not stable.\n");
              stable = 0;
              process->tempAry[(cmbox2.pid - 1)] = cmbox2.temp;
        }

        //Add up all the temps as we go for the temperature algorithm
        sumTemp += cmbox2.temp;
    }

    /*When all the processes have the same temp twice: 1) Break the loop 2) Set the messages stable field 
        to stable*/
    if(stable){
        printf("L2: Temperature Stabilized: %d\n", msgp2.temp);
        unstable = 0;
        msgp2.stable = 0;
    }
    else { //Calculate a new temp and set the temp field in the message
        int newTemp = (msgp2.temp + 1000*sumTemp) / (1000*NUM_PROCESSES + 1);
        usleep(100000);
        msgp2.temp = newTemp;
        printf("L2: The new temp in central is %d\n",newTemp);
    }

    /* Send a new message to all processes to inform of new temp or stability */ 
    for(int i = 0; i < NUM_PROCESSES; i++){
      // printf("updating group 2 processes.\n");
      result = msgsnd(process->msqid[i], &msgp2, length, 0);
    }
  }
}

//MAIN function
int main(int argc, char *argv[]) {
  struct timeval t1, t2;
  double elapsedTime;

 // start timer
 gettimeofday(&t1, NULL);

  //Validate that a temperature was given via the command line
  if(argc != 5) {
    printf("USAGE: Too few arguments --./central.out Temp1 Temp2 MBox1 MBox2");
    exit(0);
  }

  printf("\nStarting Server...\n");

  //Set up local variables
  int i,result,length,status;             //counter for loops
  int uid = 0;                               //central process ID

  int initTemp = atoi(argv[1]);            //starting temperature
  int initTemp2 = atoi(argv[2]);  

  int msqid1[NUM_PROCESSES];       //mailbox IDs for all processes
  int msqid2[NUM_PROCESSES];       
  
  int unstable = 1;          //boolean to denote temp stability
  
  int tempAry1[NUM_PROCESSES];   //array of process temperatures
  int tempAry2[NUM_PROCESSES];

  int mBox1 = atoi(argv[3]);
  int mBox2 = atoi(argv[4]);

  //Create the Central Servers Mailbox
  int msqidC1 = msgget(mBox1, 0600 | IPC_CREAT);  //34363130
  int msqidC2 = msgget(mBox2, 0600 | IPC_CREAT);  //34364135

  //Create the mailboxes for the other processes and store their IDs
  for(i = 1; i <= NUM_PROCESSES; i++){            
    msqid1[(i-1)] = msgget((mBox1 + i), 0600 | IPC_CREAT);    //3436413(1 - 4)
  }

  //Set up second mailbox, and store pid's.
  for(i = 1; i <= NUM_PROCESSES; i++){                        
    msqid2[(i-1)] = msgget((mBox2 + i), 0600 | IPC_CREAT);    //3436413(6 - 9)
  }

  //Initialize the messages to be sent
  msgp1.priority = 1;
  msgp1.pid = uid;
  msgp1.temp = initTemp;
  msgp1.stable = 1;

  msgp2.priority = 1;
  msgp2.pid = uid;
  msgp2.temp = initTemp2;
  msgp2.stable = 1;

  /* The length is essentially the size of the structure minus sizeof(mtype) */
  length = sizeof(msgp1) - sizeof(long);

  process.initTemp = initTemp2;
  process.mbox = mBox2;
  process.msqidC = msqidC2;

  for (int i = 0; i < NUM_PROCESSES; i++)
  { 
    process.msqid[i] = msqid2[i];
    process.tempAry[i] = tempAry2[i];
  }


  //Thread variable called group2
  pthread_t group2;

  //Create a thread, using 
  //var: group2, associate stabalize() function, and process struct.
  if(pthread_create(&group2, NULL, stabalize, &process))
  {
      fprintf(stderr, "Error creating thread\n");
      return 1;
  }

  //While the processes have different temperatures
  while(unstable == 1){
    int sumTemp = 0;        //sum up the temps as we loop
    int stable = 1;            //stability trap

    // Get new messages from the processes
    for(i = 0; i < NUM_PROCESSES; i++)
    {
        // printf("L1: Next Process. \n");
        result = msgrcv( msqidC1, &cmbox1, length, 1, 0);

        /* If any of the new temps are different from the old temps then we are still unstable. Set the 
            new temp to the corresponding process ID in the array */
        if(tempAry1[(cmbox1.pid - 1)] != cmbox1.temp) {
              // printf("L1: Not stable. \n");
              stable = 0;
              tempAry1[(cmbox1.pid - 1)] = cmbox1.temp;
        }

        //Add up all the temps as we go for the temperature algorithm
        sumTemp += cmbox1.temp;
    }

    /*When all the processes have the same temp twice: 1) Break the loop 2) Set the messages stable field 
        to stable*/
    if(stable){
        printf("L1: Temperature Stabilized: %d\n", msgp1.temp);
        unstable = 0;
        msgp1.stable = 0;
    }
    else { //Calculate a new temp and set the temp field in the message
        int newTemp = (msgp1.temp + 1000*sumTemp) / (1000*NUM_PROCESSES + 1);
        usleep(100000);
        msgp1.temp = newTemp;
        printf("L1: The new temp in central is %d\n",newTemp);
    }

    /* Send a new message to all processes to inform of new temp or stability */
    for(i = 0; i < NUM_PROCESSES; i++){
      // printf("updating group 1 processes.\n");
      result = msgsnd( msqid1[i], &msgp1, length, 0);
      // printf("done.\n");
    }
  }

  //To here
  if(pthread_join(group2, NULL)) {
    fprintf(stderr, "Error joining thread\n");
    return 2;
}
  // int msqid2[NUM_PROCESSES];   


  printf("\nShutting down Server...\n");

  //Remove the first group mailbox
  status = msgctl(msqidC1, IPC_RMID, 0);

  //Validate nothing when wrong when trying to remove mailbox
  if(status != 0){
    printf("\nERROR closing group 1 mailbox\n");
  }

  //Remove the second group mailbox
  status = msgctl(msqidC2, IPC_RMID, 0);

  //Validate nothing when wrong when trying to remove mailbox
  if(status != 0){
    printf("\nERROR closing group 2 mailbox\n");
  }

  // stop timer
  gettimeofday(&t2, NULL);

  // compute and print the elapsed time in millisec
  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
  printf("The elapsed time is %fms\n", elapsedTime);

  return 0;
}



