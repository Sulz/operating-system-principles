#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// #define CENTRAL_MAILBOX 3436413    //Central Mailbox number 
#define NUM_PROCESSES 4          //Total number of external processes

struct {
  long priority;          //message priority
  int temp;               //temperature
  int pid;                //process id
  int stable;             //boolean for temperature stability
} msgp1, msgp2, cmbox1, cmbox2;

//MAIN function
int main(int argc, char *argv[]) {
  struct timeval t1, t2;
  double elapsedTime;

 // start timer
 gettimeofday(&t1, NULL);

  //Validate that a temperature was given via the command line
  if(argc != 5) {
    printf("USAGE: Too few arguments --./central.out Temp1 Temp2 MBox1 MBox2");
    exit(0);
  }

  printf("\nStarting Server...\n");


  //Set up local variables
  int i,length,status;                      //counter for loops
  int uid = 0;                              //central process ID
  
  int result1;
  int result2;

  int initTemp1 = atoi(argv[1]);            //starting temperature
  int initTemp2 = atoi(argv[2]);            //starting temperature
  
  int msqid1[NUM_PROCESSES];                //mailbox IDs for all processes
  int msqid2[NUM_PROCESSES];                //mailbox IDs for all processes
  
  int unstable1 = 1;                         //boolean to denote temp stability
  int unstable2 = 1;                         //boolean to denote temp stability
  
  int tempAry1[NUM_PROCESSES];               //array of process temperatures
  int tempAry2[NUM_PROCESSES];               //array of process temperatures

  int mBox1 = atoi(argv[3]);
  int mBox2 = atoi(argv[4]);     

  //Create the Central Servers Mailbox
  int msqidC1 = msgget(mBox1, 0600 | IPC_CREAT);

  //Create the Central Servers Mailbox
  int msqidC2 = msgget(mBox2, 0600 | IPC_CREAT);

  //Create the mailboxes for the other processes and store their IDs
  for(i = 1; i <= NUM_PROCESSES; i++){
    msqid1[(i-1)] = msgget((mBox1 + i), 0600 | IPC_CREAT);
  }

  //Create the mailboxes for the other processes and store their IDs
  for(i = 1; i <= NUM_PROCESSES; i++){
    msqid2[(i-1)] = msgget((mBox2 + i), 0600 | IPC_CREAT);
  }

  //Initialize the message to be sent to first set of processes.
  msgp1.priority = 1;
  msgp1.pid = uid;
  msgp1.temp = initTemp1;
  msgp1.stable = 1;

  //Initialize the message to be sent to second set of processes.
  msgp2.priority = 1;
  msgp2.pid = uid;
  msgp2.temp = initTemp2;
  msgp2.stable = 1;

  /* The length is essentially the size of the structure minus sizeof(mtype) */
  length = sizeof(msgp1) - sizeof(long);

  printf("Made it, frsure.\n");
  //While the processes have different temperatures
  while(unstable1 == 1 || unstable2 == 1)
  {
    // printf("Entered first loop.\n");
    int sumTemp1 = 0;            //sum up the temps as we loop
    int stable1 = 0;             //stability trap

    int sumTemp2 = 0;            //sum up the temps as we loop
    int stable2 = 0;             //stability trap

    // Get new messages from the processes
    for(i = 0; i < NUM_PROCESSES; i++)
    {
      printf("Entered nested loop.\n");
      result1 = msgrcv( msqidC1, &cmbox1, length, 1, 0);
      // result2 = msgrcv( msqidC2, &cmbox2, length, 1, 0);
      printf("Got a message.\n");

      /* If any of the new temps are different from the old temps then we are still unstable. Set the 
          new temp to the corresponding process ID in the array */
      if((tempAry1[(cmbox1.pid - 1)] != cmbox1.temp))  {
        stable1 = 0;
        tempAry1[(cmbox1.pid - 1)] = cmbox1.temp;
      }

      //Add up all the temps as we go for the temperature algorithm
      sumTemp1 += cmbox1.temp;

      if((tempAry2[(cmbox2.pid - 1)] != cmbox2.temp)) {
        stable2 = 0;
        tempAry2[(cmbox2.pid - 1)] = cmbox2.temp;
      }

      //Add up all the temps as we go for the temperature algorithm
      sumTemp2 += cmbox2.temp;
    } 

    /*When all the processes have the same temp twice: 1) Break the loop 2) Set the messages stable field 
        to stable*/
    if(stable1){
        printf("Temperature Stabilized: %d\n", msgp1.temp);
        unstable1 = 0;
        msgp1.stable = 0;
    }
    else { //Calculate a new temp and set the temp field in the message
        int newTemp = (msgp1.temp + 1000*sumTemp1) / (1000*NUM_PROCESSES + 1);
        usleep(100000);
        msgp1.temp = newTemp;
        printf("The new temp in central for first set is %d\n",newTemp);
    }

    // if(stable2){
    //     printf("Temperature Stabilized: %d\n", msgp2.temp);
    //     unstable2 = 0;
    //     msgp2.stable = 0;
    // }
    // else { //Calculate a new temp and set the temp field in the message
    //     int newTemp = (msgp2.temp + 1000*sumTemp2) / (1000*NUM_PROCESSES + 1);
    //     usleep(100000);
    //     msgp2.temp = newTemp;
    //     printf("The new temp in central for second set is %d\n",newTemp);
    // }

    /* Send a new message to all processes to inform of new temp or stability */
    for(i = 0; i < NUM_PROCESSES; i++){
      result1 = msgsnd(msqid1[i], &msgp1, length, 0);
    }
    /* Send a new message to all processes to inform of new temp or stability */
    // for(i = 0; i < NUM_PROCESSES; i++){
    //   result2 = msgsnd(msqid2[i], &msgp2, length, 0);
    // }
  }

  printf("\nShutting down Server...\n");

  //Remove the mailbox
  status = msgctl(msqidC1, IPC_RMID, 0);

  //Validate nothing when wrong when trying to remove mailbox
  if(status != 0){
    printf("\nERROR closing mailbox\n");
  }


  status = msgctl(msqidC2, IPC_RMID, 0);

  //Validate nothing when wrong when trying to remove mailbox
  if(status != 0){
    printf("\nERROR closing mailbox\n");
  }

  // stop timer
  gettimeofday(&t2, NULL);

  // compute and print the elapsed time in millisec
  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
  printf("The elapsed time is %fms\n", elapsedTime);

  return 0;
}
