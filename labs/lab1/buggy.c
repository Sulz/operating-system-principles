/* 
 * This program has deliberate errors.
 * Use gdb to assist you locating them.
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{

   int i;
   int *intArray;  /* this is not an array, it is a uninitialised pointer */
   char *string;   /* this is also not an array, also uninitialised pointer */

   for (i=0; i<=10; i++)
   {
      intArray[i] = rand() % 10;  /* since intArray is uninit. this is wrong */
   }
   
   printf("''string'' is %i characters long\n", strlen(string));

   strcpy(string, "This is a medium sized string"); /* wrong, string uninit. */

   printf("''string'' is %i characters long\n", strlen(string));

   return EXIT_SUCCESS;
}