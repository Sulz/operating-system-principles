#include <omp.h>
#include <stdio.h>

int main(int argc, char* argv[])
{

  int i = 0;


  #pragma omp parallel for
  for (int i = 0; i < 10; i++)
  {
    /* code */
    printf("I'm a parallel region.\n");
    //sleep(1);
  }
  
  

  return 0;

}